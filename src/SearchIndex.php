<?php

namespace App\src;

use Firesphere\ElasticSearch\Indexes\ElasticIndex;
use Firesphere\SolrSearch\Indexes\BaseIndex;

class SearchIndex extends BaseIndex
{

    public function getIndexName(): string
    {
        return 'search-testindex';
    }
}
